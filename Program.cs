﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Net.NetworkInformation;

namespace netmon {
    class Program {
          
        static void Main(string[] args) {
            int WAIT_TIME = 1000;  

            Dictionary<string, long> timestepData = new Dictionary<string, long>();
            Dictionary<string, long> totalData = new Dictionary<string, long>();

            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();

            while (true) {
                Console.Clear();

                foreach (NetworkInterface nic in nics) {
                    if (nic.OperationalStatus == OperationalStatus.Up) {
                        long previousTimestepData = 0L;
                        if (timestepData.ContainsKey(nic.Id)) {
                            previousTimestepData = timestepData[nic.Id];
                        }

                        long result = nic.GetIPStatistics().BytesReceived - previousTimestepData;

                        System.Console.WriteLine("|-- " + nic.Description + " --|");           
                        System.Console.WriteLine("Total data received (B): " + nic.GetIPStatistics().BytesReceived);
                        System.Console.WriteLine("Total data received (KB): " + nic.GetIPStatistics().BytesReceived / 1024);
                        System.Console.WriteLine("Total data received (MB): " + nic.GetIPStatistics().BytesReceived / (1024 * 1024));
                        System.Console.WriteLine("Data received (KB/s): " + result / 1024);

                        timestepData[nic.Id] = nic.GetIPStatistics().BytesReceived;
                    }
                }

                System.Threading.Thread.Sleep(WAIT_TIME);
            }
        }
    }
}
